package tf.bug.scolor

import tf.bug.scolor.implicits._
import cats.data.State
import polymorphic.Instance
import scodec.bits.ByteVector
import spire.math.{UByte, UInt}

trait Datable[-T] {

  def alignment(t: T): UByte
  def children(t: T): BAW[List[Instance[Datable]]]
  def byteVector(t: T): BAW[ByteVector]
  def length(t: T): BAW[UInt]

}

trait DefiniteByteable[-T] extends Datable[T] {

  override def alignment(t: T): UByte = UByte(1)
  override def children(t: T): BAW[List[Instance[Datable]]] = State(i => (i, List()))
  override def length(t: T): BAW[UInt] = byteVector(t).map(l => UInt(l.length))

}
