package tf.bug.scolor

import cats.data.State
import scodec.bits.ByteVector
import spire.math.UInt

trait ByteAllocator {

  /**
    * Insert data at an offset in the byte allocator.
    *
    * @param offset The offset
    * @param data   The data
    */
  def insert[T](offset: Offset, data: T)(implicit datableEv: Datable[T]): ByteAllocator

  /**
    * Insert data at the next available offset.
    *
    * @param data The data
    */
  def insert[T](data: T)(implicit datableEv: Datable[T]): ByteAllocator = {
    val (newBA, allocOffset) = allocate(data)
    newBA.insert(allocOffset, data)
  }

  def allocate[T](data: T)(implicit datableEv: Datable[T]): (ByteAllocator, Offset) = {
    val (endBA, endL) = datableEv.length(data).run(this).value
    endBA.allocate(data, endL)
  }

  /**
    * Allocate an offset for a number of bytes.
    *
    * @param numBytes The number of bytes to allocate
    * @return an offset describing the next open space these bytes can fit
    */
  def allocate(numBytes: UInt): (ByteAllocator, Offset)

  def allocate[T](data: T, numBytes: UInt)(implicit datableEv: Datable[T]): (ByteAllocator, Offset)

  def nextOffset: Offset

  def getBytes: ByteVector

}
