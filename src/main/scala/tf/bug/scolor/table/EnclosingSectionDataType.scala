package tf.bug.scolor.table

import cats._
import cats.data._
import cats.implicits._
import scodec.bits.ByteVector
import tf.bug.scolor.implicits._
import tf.bug.scolor.ByteAllocator
import spire.math.UByte

/**
  * This is a table but without tags or etc. Used for when you need a custom data type in a table.
  */
abstract class EnclosingSectionDataType extends SectionDataType {

  def sections: BAW[List[Section]]

  override def bytes: BAW[ByteVector] = sections.flatMap(_.traverse(_.bytes).map(_.combineAll))

}
