package tf.bug.scolor.table

import cats.data.State
import scodec.bits.ByteVector
import tf.bug.scolor.{ByteAllocator, Data}
import spire.math.UByte

trait SectionDataType extends Data {

  /**
    * @return an array of unsigned bytes representing the data.
    */
  def bytes: State[ByteAllocator, ByteVector]

}
