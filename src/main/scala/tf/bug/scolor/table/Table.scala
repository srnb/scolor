package tf.bug.scolor.table

import scodec.bits.ByteVector
import tf.bug.scolor._
import spire.math.UByte

trait Table extends EnclosingSectionDataType {

  /**
    * @return the table tag/name/identifier
    */
  def name: String

  /**
    * @return the sections/partitions/rows of a table
    */
  def sections: BAW[List[Section]]

  /**
    * The bytes of table data. The data does not have to be padded.
    *
    * @return Table data as a byte array.
    */
  def bytes: BAW[ByteVector]

}
