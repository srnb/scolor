package tf.bug.scolor.table

import cats.data.State
import scodec.bits.ByteVector
import tf.bug.scolor.ByteAllocator
import spire.math.UByte

case class Section(name: String, data: SectionDataType) {

  /**
    * @return the bytes of data
    */
  def bytes: State[ByteAllocator, ByteVector] = data.bytes

}
