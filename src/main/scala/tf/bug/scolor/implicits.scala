package tf.bug.scolor
import cats.data.State
import cats.kernel.Monoid
import polymorphic.Instance
import scodec.bits.ByteVector
import spire.math.{UByte, UInt, UShort}

object implicits {

  type BAW[A] = State[ByteAllocator, A]

  implicit val byteVectorMonoid: Monoid[ByteVector] = new Monoid[ByteVector] {
    override def empty: ByteVector = ByteVector.empty
    override def combine(
      x: ByteVector,
      y: ByteVector
    ): ByteVector = x ++ y
  }

  implicit val uintMonoid: Monoid[UInt] = new Monoid[UInt] {
    override def empty: UInt = UInt(0)
    override def combine(x: UInt, y: UInt): UInt = x + y
  }

  implicit def datableToData[T](t: T)(implicit datableEv: Datable[T]): Data = new Data {

    override def length: BAW[UInt] = datableEv.length(t)

    override def bytes: BAW[ByteVector] = datableEv.byteVector(t)

    override def data: BAW[List[Data]] = datableEv.children(t).map(_.map(c => datableToData(c.first)(c.second)))

    override def alignment: UByte = datableEv.alignment(t)

  }

  implicit def dataDatable: Datable[Data] = new Datable[Data] {
    override def alignment(t: Data): UByte = t.alignment
    override def children(t: Data): BAW[List[Instance[Datable]]] =
      t.data.map(_.map((d: Data) => Instance.capture(d)(dataDatable)))
    override def byteVector(t: Data): BAW[ByteVector] = t.bytes
    override def length(t: Data): BAW[UInt] = t.length
  }

  implicit def stringByteable: DefiniteByteable[String] =
    (t: String) => State(i => (i, ByteVector(t.getBytes)))

  implicit def uShortByteable: DefiniteByteable[UShort] =
    (t: UShort) => State(i => (i, ByteVector(((t.toShort & 0xFF00) >> 8).toByte, (t.toShort & 0x00FF).toByte)))

  implicit def uIntBytable: DefiniteByteable[UInt] =
    (t: UInt) =>
      State(
        i =>
          (
            i,
            ByteVector(
              ((t.toInt & 0xFF000000) >> 24).toByte,
              ((t.toInt & 0x00FF0000) >> 16).toByte,
              ((t.toInt & 0x0000FF00) >> 8).toByte,
              (t.toInt & 0x000000FF).toByte
            )
        )
    )

  implicit def byteBytable: DefiniteByteable[Byte] = (t: Byte) => State(i => (i, ByteVector(t)))

  def allocate[T: Datable](t: T): BAW[Offset] = State(b => b.allocate(t))

}
