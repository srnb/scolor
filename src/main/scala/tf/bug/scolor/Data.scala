package tf.bug.scolor

import cats.data.State
import scodec.bits.ByteVector
import spire.math.{UByte, UInt}

trait Data {

  /**
    * Calculate/retrieve/return length in bytes of this data. Useful for if data needs to be allocated before it is calculated.
    *
    * @return an unsigned integer describing the length of this data block
    */
  def length: State[ByteAllocator, UInt]

  /**
    * @return what byte modulus this data should be aligned to
    */
  def alignment: UByte = UByte(1)

  /**
    * Get the bytes to insert at the offset the byte allocator gives you.
    *
    * @return a byte vector representing the font data.
    */
  def bytes: State[ByteAllocator, ByteVector]

  /**
    * Gets data sections if this data block has offsets. Used for if data needs to be allocated but can be in any location.
    *
    * @return a collection of Data objects
    */
  def data: State[ByteAllocator, List[Data]]

}
