package tf.bug.scolor.otf

import java.io.File

import scodec.bits.ByteVector
import tf.bug.scolor.Font
import tf.bug.scolor.table.Table

// TODO reimplement this functionally
class OpenTypeFont(tables: List[Table]) extends Font {

  /**
    * @return The bytes of the font file
    */
  override def getBytes: ByteVector = ???

  /**
    * Write a font/set of fonts to a directory. Multiple are allowed for different platforms or styles.
    *
    * @param dir  The directory
    * @param name The base name of the font
    */
  override def writeFile(dir: File, name: String): Unit = ???

}
