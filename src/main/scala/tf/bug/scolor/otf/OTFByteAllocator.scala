package tf.bug.scolor.otf

import polymorphic.Instance
import scodec.bits.ByteVector
import spire.math.{UByte, UInt}
import tf.bug.scolor.implicits._
import tf.bug.scolor.otf.types.OTFOffset32
import tf.bug.scolor.{ByteAllocator, Datable, Offset}

case class OTFByteAllocator(
  f: OpenTypeFont,
  allocMap: Map[Instance[Datable], Offset] = Map(),
  byteMap: Map[Offset, UByte] = Map(),
  nextAvailableOffset: Offset = OTFOffset32(0)
) extends ByteAllocator {

  def insert[T](offset: Offset, data: T)(implicit datableEv: Datable[T]): ByteAllocator = {
    val (newAlloc @ OTFByteAllocator(_, _, newByteMap, _), bytes) = datableEv.byteVector(data).run(this).value
    val alignment = datableEv.alignment(data)
    val top = offset.position.toInt + bytes.length
    val newBM = (offset.position.toLong until top)
      .foldLeft(newByteMap)((nm, p) => nm + (OTFOffset32(p) -> UByte(bytes(p - offset.position.toLong))))
    val adjustedOffset = OTFOffset32(
      Math.max(top + (alignment.toInt - (top % alignment.toInt)), newAlloc.nextOffset.position.toLong)
    )
    val newBA = newAlloc.copy(byteMap = newBM, nextAvailableOffset = adjustedOffset)
    val (endBA, bv) = datableEv.byteVector(data).run(newBA).value
    bv.foldLeft(endBA)((na, d) => na.insert(d))
  }

  def allocate[T](data: T, numBytes: UInt)(implicit datableEv: Datable[T]): (ByteAllocator, Offset) = {
    allocMap.get(data) match {
      case Some(o) => (this, o)
      case None => allocate(numBytes)
    }
  }

  def allocate(numBytes: UInt): (ByteAllocator, Offset) = {
    allocate(numBytes, UByte(1))
  }

  def allocate(numBytes: UInt, alignment: UByte): (ByteAllocator, Offset) = {
    val prev = nextOffset
    val unaligned = prev.position + numBytes
    val adjOffset = OTFOffset32(unaligned.toLong + (alignment.toLong - (unaligned.toLong % alignment.toLong)))
    (copy(nextAvailableOffset = adjOffset), prev)
  }

  override def nextOffset: Offset = nextAvailableOffset

  def getBytes: ByteVector = {
    val m: Long = byteMap.map { case (offset, _) => offset.position.toLong }.max
    ByteVector((0l to m).map(i => byteMap.getOrElse(OTFOffset32(i), UByte(0)).signed))
  }

}

object OTFByteAllocator {}
