package tf.bug.scolor.otf.tables.color.google

import cats.data.State
import tf.bug.scolor.{ByteAllocator, Data}
import tf.bug.scolor.otf.types.num.{OTFInt8, OTFUInt8}
import tf.bug.scolor.table.Section
import spire.math.{UByte, UInt}
import tf.bug.scolor.implicits.BAW

case class OTFGoogleSmallGlyphMetrics(
  height: UByte,
  width: UByte,
  bearingX: Byte,
  bearingY: Byte,
  advance: UByte
) extends OTFGoogleGlyphMetrics {

  override def sections: BAW[List[Section]] =
    State(
      i =>
        (
          i,
          List(
            Section("height", OTFUInt8(height)),
            Section("width", OTFUInt8(width)),
            Section("bearingX", OTFInt8(bearingX)),
            Section("bearingY", OTFInt8(bearingY)),
            Section("advance", OTFUInt8(advance))
          )
      )
    )

  /**
    * Calculate/retrieve/return length in bytes of this data. Useful for if data needs to be allocated before it is calculated.
    *
    * @return an unsigned integer describing the length of this data block
    */
  override def length: BAW[UInt] = State(i => (i, UInt(5)))

  /**
    * Gets data sections if this data block has offsets. Used for if data needs to be allocated but can be in any location.
    *
    * @return an array of Data objects
    */
  override def data: BAW[List[Data]] = State(i => (i, List()))

}
