package tf.bug.scolor.otf.tables.color.apple

import cats._
import cats.data._
import cats.implicits._
import tf.bug.scolor.implicits._
import tf.bug.scolor.{ByteAllocator, Data}
import tf.bug.scolor.otf.tables.OpenTypeTable
import tf.bug.scolor.otf.types.OTFArray
import tf.bug.scolor.otf.types.num.{OTFUInt16, OTFUInt32}
import tf.bug.scolor.table.Section
import spire.math.{UInt, UShort}

case class OTFSBIXTable(
  sds: List[OTFAppleStrikeData]
) extends OpenTypeTable {

  /**
    * @return the table tag/name/identifier
    */
  override def name = "sbix"

  private val dataOffsets: BAW[List[UInt]] =
    (allocate(this), data).mapN { (myPos, strikes) =>
      strikes.traverse { strike =>
        allocate(strike).map(_.position - myPos.position)
      }
    }.flatten

  /**
    * @return the sections/partitions/rows of a table
    */
  override def sections: BAW[List[Section]] =
    for {
      strikeOffsets <- dataOffsets
    } yield
      List(
        Section("version", OTFUInt16(UShort(1))),
        Section("flags", OTFUInt16(UShort(2))),
        Section("numStrikes", OTFUInt32(UInt(sds.size))),
        Section("offsets", OTFArray(strikeOffsets.map(OTFUInt32)))
      )

  /**
    * Gets data sections if this data block has offsets. Used for if data needs to be allocated but can be in any location.
    *
    * @return an array of Data objects
    */
  override def data: BAW[List[Data]] = State(i => (i, sds))

  /**
    * Calculate/retrieve/return length in bytes of this data. Useful for if data needs to be allocated before it is calculated.
    *
    * @return an unsigned integer describing the length of this data block
    */
  override def length: BAW[UInt] = State(i => (i, UInt(8) + (UInt(4) * UInt(sds.size))))

}
