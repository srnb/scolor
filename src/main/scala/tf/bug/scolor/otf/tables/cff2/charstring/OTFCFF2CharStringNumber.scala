package tf.bug.scolor.otf.tables.cff2.charstring

import cats.data.State
import scodec.bits.ByteVector
import tf.bug.scolor.otf.types.num.OTFInt16
import tf.bug.scolor.{ByteAllocator, Data}
import tf.bug.scolor.table.SectionDataType
import spire.math.{UByte, UInt}
import tf.bug.scolor.implicits.BAW

class OTFCFF2CharStringNumber(n: Short) extends SectionDataType {

  val byteVec: ByteVector = if (n >= -107 && n <= 107) {
    ByteVector(n.toByte + 137)
  } else if (n >= 108 && n <= 1131) {
    val v = ((n - 108.0) / 256.0).floor.toInt + 247
    val w = n - (((v - 247) * 256) + 108)
    ByteVector(v, w)
  } else if (n <= -108 && n >= -1131) {
    val v = -((n + 108.0) / 256.0).floor.toInt + 251
    val w = n - ((-(v - 251) * 256) - 108)
    ByteVector(v, w)
  } else {
    ByteVector(28) ++ OTFInt16.bytes(n)
  }

  /**
    * @return an array of unsigned bytes representing the data.
    */
  override def bytes: BAW[ByteVector] = State(i => (i, byteVec))

  /**
    * Calculate/retrieve/return length in bytes of this data. Useful for if data needs to be allocated before it is calculated.
    *
    * @return an unsigned integer describing the length of this data block
    */
  override def length: BAW[UInt] = State(i => (i, UInt(byteVec.length)))

  /**
    * Gets data sections if this data block has offsets. Used for if data needs to be allocated but can be in any location.
    *
    * @return an array of Data objects
    */
  override def data: BAW[List[Data]] = State(i => (i, List()))

}
