package tf.bug.scolor.otf.tables.color.apple

import cats._
import cats.data._
import cats.implicits._
import tf.bug.scolor.implicits._
import tf.bug.scolor.{ByteAllocator, Data}
import tf.bug.scolor.otf.tables.color.OTFsRGBPNG
import tf.bug.scolor.otf.types.OTFString
import tf.bug.scolor.otf.types.num.OTFInt16
import tf.bug.scolor.table.{EnclosingSectionDataType, Section}
import spire.math.UInt

case class OTFAppleGlyphData(
  originOffsetX: Short,
  originOffsetY: Short,
  img: OTFsRGBPNG
) extends EnclosingSectionDataType {

  override def sections: BAW[List[Section]] =
    State(
      i =>
        (
          i,
          List(
            Section("originOffsetX", OTFInt16(originOffsetX)),
            Section("originOffsetY", OTFInt16(originOffsetY)),
            Section("imgType", OTFString("png ")),
            Section("imgData", img)
          )
      )
    )

  /**
    * Calculate/retrieve/return length in bytes of this data. Useful for if data needs to be allocated before it is calculated.
    *
    * @return an unsigned integer describing the length of this data block
    */
  override def length: BAW[UInt] = img.length.map(_ + UInt(8))

  /**
    * Gets data sections if this data block has offsets. Used for if data needs to be allocated but can be in any location.
    *
    * @return an array of Data objects
    */
  override def data: BAW[List[Data]] = State(i => (i, List()))

}
