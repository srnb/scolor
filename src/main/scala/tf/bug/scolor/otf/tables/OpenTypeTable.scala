package tf.bug.scolor.otf.tables

import cats._
import cats.data._
import cats.implicits._
import scodec.bits.ByteVector
import tf.bug.scolor.implicits._
import tf.bug.scolor.table.Table
import tf.bug.scolor.{ByteAllocator, Offset}
import spire.math.UByte

abstract class OpenTypeTable extends Table {

  override def bytes: BAW[ByteVector] = sections.flatMap(_.traverse(_.bytes)).map(_.combineAll)

  def position: BAW[Offset] = allocate(this)

  override def alignment: UByte = UByte(4)

}
