package tf.bug.scolor.otf.tables.color.google

import cats._
import cats.data._
import cats.implicits._
import tf.bug.scolor.implicits._
import tf.bug.scolor.{ByteAllocator, Data}
import tf.bug.scolor.otf.tables.color.OTFsRGBPNG
import tf.bug.scolor.table.{EnclosingSectionDataType, Section}
import spire.math.UInt

case class OTFGoogleGlyphData(
  metrics: OTFGoogleGlyphMetrics,
  img: OTFsRGBPNG
) extends EnclosingSectionDataType {

  override def sections: BAW[List[Section]] =
    State(
      i =>
        (
          i,
          List(
            Section("metrics", metrics),
            Section("data", img)
          )
      )
    )

  /**
    * Calculate/retrieve/return length in bytes of this data. Useful for if data needs to be allocated before it is calculated.
    *
    * @return an unsigned integer describing the length of this data block
    */
  override def length: BAW[UInt] =
    for {
      ml <- metrics.length
      il <- img.length
    } yield ml + il

  /**
    * Gets data sections if this data block has offsets. Used for if data needs to be allocated but can be in any location.
    *
    * @return an array of Data objects
    */
  override def data: BAW[List[Data]] = State(i => (i, List()))

}
