package tf.bug.scolor.otf.tables

import cats.data.State
import tf.bug.scolor.otf.types.OTFFixedPoint
import tf.bug.scolor.otf.types.num.OTFUInt16
import tf.bug.scolor.table.Section
import tf.bug.scolor.{ByteAllocator, Data}
import spire.math.{UInt, UShort}
import tf.bug.scolor.implicits.BAW

case class OTFMaxPTable(numGlyphs: UShort) extends OpenTypeTable {

  /**
    * @return the table tag/name/identifier
    */
  override def name: String = "maxp"

  /**
    * @return the sections/partitions/rows of a table
    */
  override def sections: BAW[List[Section]] =
    State(
      i =>
        (
          i,
          List(
            Section("version", OTFFixedPoint(0.3125)),
            Section("numGlyphs", OTFUInt16(numGlyphs))
          )
      )
    )

  /**
    * Calculate/retrieve/return length in bytes of this data. Useful for if data needs to be allocated before it is calculated.
    *
    * @return an unsigned integer describing the length of this data block
    */
  override def length: BAW[UInt] = State(i => (i, UInt(6)))

  /**
    * Gets data sections if this data block has offsets. Used for if data needs to be allocated but can be in any location.
    *
    * @return an array of Data objects
    */
  override def data: BAW[List[Data]] = State(i => (i, List()))
}
