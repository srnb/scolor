package tf.bug.scolor.otf.tables

import cats._
import cats.data._
import cats.implicits._
import tf.bug.scolor.implicits._
import tf.bug.scolor.otf.types._
import tf.bug.scolor.otf.types.num.OTFUInt16
import tf.bug.scolor.table.Section
import tf.bug.scolor.{ByteAllocator, Data}
import spire.math.{UInt, UShort}

case class OTFNameTable(
  records: List[OTFNameRecord]
) extends OpenTypeTable {

  val strData = OTFArray(records.map(r => OTFString(r.data.s)))
  private val tabledRecords = records.map(_(this))
  private val recArr = OTFArray(tabledRecords)

  override def name = "name"

  override def sections: BAW[List[Section]] =
    for {
      off <- allocate(this)
      strOff <- allocate(strData)
    } yield
      List(
        Section("format", OTFUInt16(UShort(0))),
        Section("count", OTFUInt16(UShort(records.size))),
        Section("stringOffset", OTFOffset16((strOff.position - off.position).toInt)),
        Section("nameRecords", recArr)
      )

  override def length: BAW[UInt] = recArr.length.map(_ + UInt(6)).flatMap(c => strData.length.map(_ + c))

  /**
    * Gets data sections if this data block has offsets
    *
    * @return an array of Data objects
    */
  override def data: BAW[List[Data]] = State(i => (i, List(strData)))

}
