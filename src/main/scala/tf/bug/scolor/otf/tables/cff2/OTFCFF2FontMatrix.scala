package tf.bug.scolor.otf.tables.cff2

import scodec.bits.ByteVector
import tf.bug.scolor.{ByteAllocator, Data}
import tf.bug.scolor.table.SectionDataType
import spire.math.{UByte, UInt, UShort}
import cats._
import cats.data._
import cats.implicits._
import tf.bug.scolor.implicits._

case class OTFCFF2FontMatrix(
  unitsPerEm: UShort
) extends SectionDataType {

  val inverseUPEmNibble = OTFCFF2Nibble(BigDecimal(1) / BigDecimal(unitsPerEm.toBigInt))
  val op = ByteVector(0x0c, 0x07)

  val tail = List(
    inverseUPEmNibble,
    OTFCFF2Nibble.zero,
    OTFCFF2Nibble.zero,
    inverseUPEmNibble,
    OTFCFF2Nibble.zero,
    OTFCFF2Nibble.zero
  )

  /**
    * @return an array of unsigned bytes representing the data.
    */
  override def bytes: BAW[ByteVector] = tail.traverse(_.bytes).map(op ++ _.combineAll)

  /**
    * Calculate/retrieve/return length in bytes of this data. Useful for if data needs to be allocated before it is calculated.
    *
    * @return an unsigned integer describing the length of this data block
    */
  override def length: BAW[UInt] = inverseUPEmNibble.bytes.map(b => UInt(2 * b.length) + UInt(6))

  /**
    * Gets data sections if this data block has offsets. Used for if data needs to be allocated but can be in any location.
    *
    * @return an array of Data objects
    */
  override def data: BAW[List[Data]] = State(i => (i, List()))

}
