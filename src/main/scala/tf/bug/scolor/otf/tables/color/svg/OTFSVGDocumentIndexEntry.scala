package tf.bug.scolor.otf.tables.color.svg

import cats.data.State
import tf.bug.scolor.implicits._
import tf.bug.scolor.otf.types.OTFOffset32
import tf.bug.scolor.otf.types.num.{OTFUInt16, OTFUInt32}
import tf.bug.scolor.table.{EnclosingSectionDataType, RequireEnclosingSectionDataType, Section}
import tf.bug.scolor.{ByteAllocator, Data}
import spire.math.UInt

case class DatadOTFSVGDocumentIndexEntry(
  documentIndex: OTFSVGDocumentIndex,
  startGlyphID: OTFUInt16,
  endGlyphID: OTFUInt16,
  svgDocument: OTFSVGDocument
) extends EnclosingSectionDataType {

  override def sections: BAW[List[Section]] =
    for {
      l <- svgDocument.length
      docIndOff <- allocate(documentIndex)
      svgDocOff <- allocate(svgDocument)
    } yield
      List(
        Section("startGlyphID", startGlyphID),
        Section("endGlyphID", endGlyphID),
        Section(
          "svgDocOffset",
          OTFOffset32((svgDocOff.position - docIndOff.position).toLong)
        ),
        Section("svgDocLength", OTFUInt32(l))
      )

  /**
    * Calculate/retrieve/return length in bytes of this data. Useful for if data needs to be allocated before it is calculated.
    *
    * @return an unsigned integer describing the length of this data block
    */
  override def length: BAW[UInt] = State(i => (i, UInt(8)))

  /**
    * Gets data sections if this data block has offsets. Used for if data needs to be allocated but can be in any location.
    *
    * @return an array of Data objects
    */
  override def data: BAW[List[Data]] = State(i => (i, List(svgDocument)))

}

case class OTFSVGDocumentIndexEntry(
  startGlyphID: OTFUInt16,
  endGlyphID: OTFUInt16,
  svgDocument: OTFSVGDocument
) extends RequireEnclosingSectionDataType[OTFSVGDocumentIndex, DatadOTFSVGDocumentIndexEntry] {

  override def apply(t: OTFSVGDocumentIndex): DatadOTFSVGDocumentIndexEntry = {
    DatadOTFSVGDocumentIndexEntry(t, startGlyphID, endGlyphID, svgDocument)
  }

}
