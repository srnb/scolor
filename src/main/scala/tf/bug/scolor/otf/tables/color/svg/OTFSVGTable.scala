package tf.bug.scolor.otf.tables.color.svg

import cats.data.State
import tf.bug.scolor.implicits._
import tf.bug.scolor.otf.tables.OpenTypeTable
import tf.bug.scolor.otf.types.OTFOffset32
import tf.bug.scolor.otf.types.num.{OTFUInt16, OTFUInt32}
import tf.bug.scolor.table.Section
import tf.bug.scolor.{ByteAllocator, Data}
import spire.math.{UInt, UShort}

case class OTFSVGTable(
  documentIndex: OTFSVGDocumentIndex
) extends OpenTypeTable {

  /**
    * @return the table tag/name/identifier
    */
  override def name: String = "SVG "

  /**
    * @return the sections/partitions/rows of a table
    */
  override def sections: BAW[List[Section]] =
    for {
      myOff <- allocate(this)
      indOff <- allocate(this)
    } yield
      List(
        Section("version", OTFUInt16(UShort(0))),
        Section("svgDocIndexOffset", OTFOffset32((indOff.position - myOff.position).toLong)),
        Section("reserved", OTFUInt32(UInt(0)))
      )

  /**
    * Calculate/retrieve/return length in bytes of this data. Useful for if data needs to be allocated before it is calculated.
    *
    * @return an unsigned integer describing the length of this data block
    */
  override def length: BAW[UInt] = State(i => (i, UInt(10)))

  /**
    * Gets data sections if this data block has offsets. Used for if data needs to be allocated but can be in any location.
    *
    * @return an array of Data objects
    */
  override def data: BAW[List[Data]] = State(i => (i, List(documentIndex)))

}
