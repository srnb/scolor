package tf.bug.scolor.otf.tables.os2

import cats.data.State
import tf.bug.scolor.implicits._
import scodec.bits.ByteVector
import tf.bug.scolor.otf.types.OTFArray
import tf.bug.scolor.otf.types.num.OTFUInt8
import tf.bug.scolor.table.SectionDataType
import tf.bug.scolor.{ByteAllocator, Data}
import spire.math.{UByte, UInt}

case class OTFPanoseClassification(
  bFamilyType: UByte,
  bSerifStyle: UByte,
  bWeight: UByte,
  bProportion: UByte,
  bContrast: UByte,
  bStrokeVariation: UByte,
  bArmStyle: UByte,
  bLetterForm: UByte,
  bMidline: UByte,
  bXHeight: UByte
) extends SectionDataType {

  /**
    * @return an array of unsigned bytes representing the data.
    */
  override def bytes: BAW[ByteVector] =
    OTFArray(
      List(
        bFamilyType,
        bSerifStyle,
        bWeight,
        bProportion,
        bContrast,
        bStrokeVariation,
        bArmStyle,
        bLetterForm,
        bMidline,
        bXHeight
      ).map(OTFUInt8)
    ).bytes

  /**
    * Calculate/retrieve/return length in bytes of this data. Useful for if data needs to be allocated before it is calculated.
    *
    * @return an unsigned integer describing the length of this data block
    */
  override def length: BAW[UInt] = State(i => (i, UInt(10)))

  /**
    * Gets data sections if this data block has offsets. Used for if data needs to be allocated but can be in any location.
    *
    * @return an array of Data objects
    */
  override def data: BAW[List[Data]] = State(i => (i, List()))

}
