package tf.bug.scolor.otf.tables

import cats.data.State
import tf.bug.scolor.otf.types.OTFFixedPoint
import tf.bug.scolor.otf.types.num.{OTFInt16, OTFUInt32}
import tf.bug.scolor.table.Section
import tf.bug.scolor.{ByteAllocator, Data}
import spire.math.UInt
import tf.bug.scolor.implicits.BAW

case class OTFPostTable(
  italicAngle: Double,
  underlinePosition: Short,
  underlineThickness: Short,
  isFixedPitch: UInt = UInt(0),
  minMemType42: UInt = UInt(0),
  maxMemType42: UInt = UInt(0),
  minMemType1: UInt = UInt(0),
  maxMemType1: UInt = UInt(0)
) extends OpenTypeTable {

  /**
    * @return the table tag/name/identifier
    */
  override def name: String = "post"

  /**
    * @return the sections/partitions/rows of a table
    */
  override def sections: BAW[List[Section]] =
    State(
      i =>
        (
          i,
          List(
            Section("version", OTFFixedPoint(3.0)),
            Section("italicAngle", OTFFixedPoint(italicAngle)),
            Section("underlinePosition", OTFInt16(underlinePosition)),
            Section("underlineThickness", OTFInt16(underlineThickness)),
            Section("isFixedPitch", OTFUInt32(isFixedPitch)),
            Section("minMemType42", OTFUInt32(minMemType42)),
            Section("maxMemType42", OTFUInt32(maxMemType42)),
            Section("minMemType1", OTFUInt32(minMemType1)),
            Section("maxMemType1", OTFUInt32(maxMemType1))
          )
      )
    )

  /**
    * Calculate/retrieve/return length in bytes of this data. Useful for if data needs to be allocated before it is calculated.
    *
    * @return an unsigned integer describing the length of this data block
    */
  override def length: BAW[UInt] = State(i => (i, UInt(32)))

  /**
    * Gets data sections if this data block has offsets. Used for if data needs to be allocated but can be in any location.
    *
    * @return an array of Data objects
    */
  override def data: BAW[List[Data]] = State(i => (i, List()))
}
