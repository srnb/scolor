package tf.bug.scolor.otf.tables.color.google

import cats._
import cats.data._
import cats.implicits._
import tf.bug.scolor.implicits._
import tf.bug.scolor.{ByteAllocator, Data}
import tf.bug.scolor.otf.tables.OpenTypeTable
import tf.bug.scolor.otf.types._
import tf.bug.scolor.otf.types.num.OTFUInt16
import tf.bug.scolor.table.Section
import spire.math.{UInt, UShort}
import spire.syntax.std.array._

case class OTFCBDTTable(
  gds: List[OTFGoogleGlyphData]
) extends OpenTypeTable {

  override def name = "CBDT"

  private val gdsArr = OTFArray(gds)

  override def sections: BAW[List[Section]] =
    State(
      i =>
        (
          i,
          List(
            Section("majorVersion", OTFUInt16(UShort(3))),
            Section("minorVersion", OTFUInt16(UShort(0))),
            Section("data", gdsArr)
          )
      )
    )

  override def length: BAW[UInt] = gdsArr.length.map(_ + UInt(4))

  /**
    * Gets data sections if this data block has offsets
    *
    * @return an array of Data objects
    */
  override def data: BAW[List[Data]] = State(i => (i, List()))

}
