package tf.bug.scolor.otf.tables

import cats._
import cats.data._
import cats.implicits._
import tf.bug.scolor.implicits._
import tf.bug.scolor.{ByteAllocator, Data}
import tf.bug.scolor.otf.types.num.OTFUInt16
import tf.bug.scolor.otf.types.{OTFArray, OTFEncodingRecord, TabledEncodingRecord}
import tf.bug.scolor.table.Section
import spire.math.{UInt, UShort}

case class OTFCMapTable(
  encodingRecords: List[OTFEncodingRecord]
) extends OpenTypeTable {

  private val tabledRecords: List[TabledEncodingRecord] = encodingRecords.map(_(this))
  private val recArr: OTFArray[TabledEncodingRecord] = OTFArray(tabledRecords)

  override def name = "cmap"

  override def length: BAW[UInt] = recArr.length

  override def sections: BAW[List[Section]] =
    State(
      i =>
        (
          i,
          List(
            Section("version", OTFUInt16(UShort(0))),
            Section("numTables", OTFUInt16(UShort(encodingRecords.size))),
            Section("data", recArr)
          )
      )
    )

  /**
    * Gets data sections if this data block has offsets
    *
    * @return an array of Data objects
    */
  override def data: BAW[List[Data]] = State(i => (i, List()))

}
