package tf.bug.scolor.otf.tables.cff2

import cats.data.State
import tf.bug.scolor.implicits._
import tf.bug.scolor.otf.tables.cff2.charstring.OTFCFF2CharStrings
import tf.bug.scolor.table.{EnclosingSectionDataType, RequireTable, Section}
import tf.bug.scolor.{ByteAllocator, Data}
import spire.math.{UInt, UShort}

case class TabledOTFCFF2TopDict(
  table: OTFCFF2Table,
  unitsPerEm: UShort,
  charStrings: OTFCFF2CharStrings
) extends EnclosingSectionDataType {

  override def sections: BAW[List[Section]] =
    for {
      tableOffset <- allocate(table)
      myOffset <- allocate(this)
      charStringOffset <- allocate(charStrings)
    } yield
      List(
        Section("FontMatrix", OTFCFF2FontMatrix(unitsPerEm)),
        // TODO
      )

  /**
    * Calculate/retrieve/return length in bytes of this data. Useful for if data needs to be allocated before it is calculated.
    *
    * @return an unsigned integer describing the length of this data block
    */
  override def length: BAW[UInt] = ???

  /**
    * Gets data sections if this data block has offsets. Used for if data needs to be allocated but can be in any location.
    *
    * @return an array of Data objects
    */
  override def data: BAW[List[Data]] = State(i => (i, List()))

}

case class OTFCFF2TopDict(
  unitsPerEm: UShort,
  charStrings: OTFCFF2CharStrings
) extends RequireTable[OTFCFF2Table, TabledOTFCFF2TopDict] {

  override def apply(t: OTFCFF2Table): TabledOTFCFF2TopDict = {
    TabledOTFCFF2TopDict(t, unitsPerEm, charStrings)
  }

}
