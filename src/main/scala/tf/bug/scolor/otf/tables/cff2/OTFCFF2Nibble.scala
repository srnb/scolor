package tf.bug.scolor.otf.tables.cff2

import cats.data.State
import scodec.bits.ByteVector
import tf.bug.scolor.table.SectionDataType
import tf.bug.scolor.{ByteAllocator, Data}
import spire.math.{UByte, UInt}
import tf.bug.scolor.implicits.BAW

case class OTFCFF2Nibble(
  iv: BigDecimal
) extends SectionDataType {

  val ups: String = "1e" + iv
    .toString()
    .map {
      case c if ('0' to '9').contains(c) => c
      case '.' => 'a'
      case '-' => 'e'
      case 'E' => 'b'
    }
    .replace("be", "c")

  val ps: String = ups.length match {
    case e if e % 2 == 0 => ups + "ff"
    case o if o % 2 == 1 => ups + "f"
  }
  val ba: ByteVector = ByteVector(ps.grouped(2).map(s => Integer.valueOf(s, 16).toByte))

  /**
    * @return an array of unsigned bytes representing the data.
    */
  override def bytes: BAW[ByteVector] = State(i => (i, ba))

  /**
    * Calculate/retrieve/return length in bytes of this data. Useful for if data needs to be allocated before it is calculated.
    *
    * @param b The byte allocator
    * @return an unsigned integer describing the length of this data block
    */
  override def length: BAW[UInt] = State(i => (i, UInt(ba.length)))

  /**
    * Gets data sections if this data block has offsets. Used for if data needs to be allocated but can be in any location.
    *
    * @return an array of Data objects
    */
  override def data: BAW[List[Data]] = State(i => (i, List()))

}

object OTFCFF2Nibble {

  val zero = new OTFCFF2Nibble(BigDecimal(0))

}
