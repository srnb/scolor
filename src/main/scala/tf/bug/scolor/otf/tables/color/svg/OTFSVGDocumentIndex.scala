package tf.bug.scolor.otf.tables.color.svg

import cats._
import cats.data._
import cats.implicits._
import tf.bug.scolor.implicits._
import tf.bug.scolor.otf.types.OTFArray
import tf.bug.scolor.otf.types.num.OTFUInt16
import tf.bug.scolor.table.{EnclosingSectionDataType, Section}
import tf.bug.scolor.{ByteAllocator, Data}
import spire.math.{UInt, UShort}

case class OTFSVGDocumentIndex(
  entries: List[OTFSVGDocumentIndexEntry]
) extends EnclosingSectionDataType {

  private val mappedEntries = entries.map(_.apply(this))

  override def sections: BAW[List[Section]] =
    State(
      i =>
        (
          i,
          List(
            Section("numEntries", OTFUInt16(UShort(mappedEntries.size))),
            Section("entries", OTFArray(mappedEntries))
          )
      )
    )

  /**
    * Calculate/retrieve/return length in bytes of this data. Useful for if data needs to be allocated before it is calculated.
    *
    * @param b The byte allocator
    * @return an unsigned integer describing the length of this data block
    */
  override def length: BAW[UInt] = {
    val states: List[BAW[UInt]] = mappedEntries.map(_.length)
    val traversed: BAW[List[UInt]] = states.sequence
    val combined: BAW[UInt] = traversed.map(_.combineAll)
    combined.map(_ + UInt(2))
  }

  /**
    * Gets data sections if this data block has offsets. Used for if data needs to be allocated but can be in any location.
    *
    * @return an array of Data objects
    */
  override def data: BAW[List[Data]] = State(i => (i, mappedEntries))

}
