package tf.bug.scolor.otf.tables.color

import java.awt.image.BufferedImage
import java.io.ByteArrayOutputStream

import cats._
import cats.data._
import cats.implicits._
import tf.bug.scolor.implicits._
import tf.bug.scolor.{ByteAllocator, Data}
import tf.bug.scolor.table.SectionDataType
import javax.imageio.ImageIO
import scodec.bits.ByteVector
import spire.math.{UByte, UInt}

case class OTFsRGBPNG(i: BufferedImage) extends SectionDataType {

  /**
    * Calculate/retrieve/return length in bytes of this data. Useful for if data needs to be allocated before it is calculated.
    *
    * @return an unsigned integer describing the length of this data block
    */
  override def length: BAW[UInt] = bytes.map(l => UInt(l.length))

  /**
    * @return an array of unsigned bytes representing the data.
    */
  override def bytes: BAW[ByteVector] =
    State(
      b =>
        (b, {
          val baos = new ByteArrayOutputStream()
          ImageIO.write(i, "png", baos)
          baos.close()
          ByteVector(baos.toByteArray)
        })
    )

  /**
    * Gets data sections if this data block has offsets. Used for if data needs to be allocated but can be in any location.
    *
    * @return an array of Data objects
    */
  override def data: BAW[List[Data]] = State(i => (i, List()))

}
