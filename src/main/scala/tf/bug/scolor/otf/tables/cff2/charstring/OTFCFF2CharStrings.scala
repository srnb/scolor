package tf.bug.scolor.otf.tables.cff2.charstring

import tf.bug.scolor.{ByteAllocator, Data}
import tf.bug.scolor.table.{EnclosingSectionDataType, Section}
import spire.math.UInt
import tf.bug.scolor.implicits.BAW

case class OTFCFF2CharStrings(
  ) extends EnclosingSectionDataType {

  override def sections: BAW[List[Section]] = ???

  /**
    * Calculate/retrieve/return length in bytes of this data. Useful for if data needs to be allocated before it is calculated.
    *
    * @return an unsigned integer describing the length of this data block
    */
  override def length: BAW[UInt] = ???

  /**
    * Gets data sections if this data block has offsets. Used for if data needs to be allocated but can be in any location.
    *
    * @return an array of Data objects
    */
  override def data: BAW[List[Data]] = ???

}
