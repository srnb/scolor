package tf.bug.scolor.otf.tables.cff2

import cats._
import cats.data._
import cats.implicits._
import tf.bug.scolor.implicits._
import tf.bug.scolor.{ByteAllocator, Data}
import tf.bug.scolor.otf.tables.OpenTypeTable
import tf.bug.scolor.otf.types.num.{OTFUInt16, OTFUInt8}
import tf.bug.scolor.table.Section
import spire.math.{UByte, UInt, UShort}

case class OTFCFF2Table(
  dict: OTFCFF2TopDict
) extends OpenTypeTable {

  val tabledDict = dict(this)

  /**
    * @return the table tag/name/identifier
    */
  override def name: String = "CFF2"

  /**
    * @return the sections/partitions/rows of a table
    */
  override def sections: BAW[List[Section]] =
    for {
      tabledDictLength <- tabledDict.length
    } yield
      List(
        Section("majorVersion", OTFUInt8(UByte(2))),
        Section("minorVersion", OTFUInt8(UByte(0))),
        Section("headerSize", OTFUInt8(UByte(5))),
        Section("topDictLength", OTFUInt16(UShort(tabledDictLength.toInt))),
        Section("topDict", tabledDict)
      )

  /**
    * Calculate/retrieve/return length in bytes of this data. Useful for if data needs to be allocated before it is calculated.
    *
    * @return an unsigned integer describing the length of this data block
    */
  override def length: BAW[UInt] = ???

  /**
    * Gets data sections if this data block has offsets. Used for if data needs to be allocated but can be in any location.
    *
    * @return an array of Data objects
    */
  override def data: BAW[List[Data]] = ???

}
