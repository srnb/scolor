package tf.bug.scolor.otf.tables.color.svg

import cats.data.State
import tf.bug.scolor.implicits._
import tf.bug.scolor.{ByteAllocator, Data, StringableDocument}
import org.w3c.dom.Document
import scodec.bits.ByteVector
import spire.math.{UByte, UInt}

case class OTFSVGDocument(
  svg: Document
) extends Data {

  /**
    * Calculate/retrieve/return length in bytes of this data. Useful for if data needs to be allocated before it is calculated.
    *
    * @return an unsigned integer describing the length of this data block
    */
  override def length: BAW[UInt] = State(i => (i, UInt(svg.toXmlString.length)))

  /**
    * Get the bytes to insert at the offset the byte allocator gives you.
    *
    * @return an array of unsigned bytes representing the font data.
    */
  override def bytes: BAW[ByteVector] = State(i => (i, ByteVector(svg.toXmlString.getBytes("UTF-8"))))

  /**
    * Gets data sections if this data block has offsets. Used for if data needs to be allocated but can be in any location.
    *
    * @return an array of Data objects
    */
  override def data: BAW[List[Data]] = State(i => (i, List()))

}
