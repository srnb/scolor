package tf.bug.scolor.otf.types

import cats.data.State
import scodec.bits.ByteVector
import tf.bug.scolor.ByteAllocator
import spire.math.{UByte, UInt}
import tf.bug.scolor.implicits.BAW

class OTFMacString(s: String) extends OTFString(s) {

  override def length: BAW[UInt] = State(i => (i, UInt(s.getBytes("UTF-8").length)))

  override def bytes: BAW[ByteVector] = State(i => (i, ByteVector(s.getBytes("UTF-8"))))

}
