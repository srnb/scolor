package tf.bug.scolor.otf.types

import cats.data.State
import scodec.bits.ByteVector
import tf.bug.scolor.implicits._
import tf.bug.scolor.{ByteAllocator, Data}
import tf.bug.scolor.table.SectionDataType
import spire.math.{UByte, UInt}

case class OTFString(s: String) extends SectionDataType {

  override def length: BAW[UInt] = State(b => (b, UInt(s.getBytes("UTF-16BE").length)))

  /**
    * Gets data sections if this data block has offsets
    *
    * @return an array of Data objects
    */
  override def data: BAW[List[Data]] = State(i => (i, List()))

  /**
    * @return an array of unsigned bytes representing the data.
    */
  override def bytes: BAW[ByteVector] = State(i => (i, ByteVector(s.getBytes("UTF-16BE"))))

}
