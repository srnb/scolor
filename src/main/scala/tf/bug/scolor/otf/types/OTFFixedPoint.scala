package tf.bug.scolor.otf.types

import cats.data.State
import scodec.bits.ByteVector
import tf.bug.scolor.otf.types.num.OTFUInt32
import tf.bug.scolor.table.SectionDataType
import tf.bug.scolor.{ByteAllocator, Data}
import spire.math.{UByte, UInt}
import tf.bug.scolor.implicits.BAW

case class OTFFixedPoint(float: Double) extends SectionDataType {

  private val internal = OTFUInt32(UInt((float * (1 << 16)).toInt))

  /**
    * @return an array of unsigned bytes representing the data.
    */
  override def bytes: BAW[ByteVector] = internal.bytes

  /**
    * Calculate/retrieve/return length in bytes of this data. Useful for if data needs to be allocated before it is calculated.
    *
    * @return an unsigned integer describing the length of this data block
    */
  override def length: BAW[UInt] = State(i => (i, UInt(4)))

  /**
    * Gets data sections if this data block has offsets. Used for if data needs to be allocated but can be in any location.
    *
    * @return an array of Data objects
    */
  override def data: BAW[List[Data]] = State(i => (i, List()))

}
