package tf.bug.scolor.otf.types.num

import cats.data.State
import scodec.bits.ByteVector
import tf.bug.scolor.{ByteAllocator, Data}
import tf.bug.scolor.table.SectionDataType
import spire.math.{UByte, UInt, UShort}
import tf.bug.scolor.implicits.BAW

case class OTFUInt16(value: UShort) extends SectionDataType {

  override def bytes: BAW[ByteVector] =
    State(i => (i, ByteVector(((value.toShort & 0xFF00) >> 8).toByte, (value.toShort & 0x00FF).toByte)))

  override def length: BAW[UInt] = State(i => (i, UInt(2)))

  /**
    * Gets data sections if this data block has offsets
    *
    * @return an array of Data objects
    */
  override def data: BAW[List[Data]] = State(i => (i, List()))
}
