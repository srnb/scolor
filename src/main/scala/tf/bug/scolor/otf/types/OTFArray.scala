package tf.bug.scolor.otf.types

import cats._
import cats.data._
import cats.implicits._
import tf.bug.scolor.implicits._
import scodec.bits.ByteVector
import tf.bug.scolor.table.SectionDataType
import tf.bug.scolor.{ByteAllocator, Data}
import spire.math.{UByte, UInt}

case class OTFArray[T <: Data](elems: List[T]) extends SectionDataType {

  override def length: BAW[UInt] = {
    val states: List[BAW[UInt]] = elems.map(_.length)
    val traversed: BAW[List[UInt]] = states.sequence
    val combined: BAW[UInt] = traversed.map(_.combineAll)
    combined
  }

  override def bytes: BAW[ByteVector] = {
    val states: List[BAW[ByteVector]] = elems.map(_.bytes)
    val traversed: BAW[List[ByteVector]] = states.sequence
    val combined: BAW[ByteVector] = traversed.map(_.combineAll)
    combined
  }

  /**
    * Gets data sections if this data block has offsets
    *
    * @return an array of Data objects
    */
  override def data: BAW[List[Data]] = State(i => (i, List()))

}
