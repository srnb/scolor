package tf.bug.scolor.otf.types.num

import cats.data.State
import scodec.bits.ByteVector
import tf.bug.scolor.{ByteAllocator, Data}
import tf.bug.scolor.table.SectionDataType
import spire.math.{UByte, UInt}
import tf.bug.scolor.implicits.BAW

case class OTFUInt32(value: UInt) extends SectionDataType {

  override def bytes: BAW[ByteVector] =
    State(
      i =>
        (i, {
          ByteVector(
            ((value.toInt & 0xFF000000) >> 24).toByte,
            ((value.toInt & 0x00FF0000) >> 16).toByte,
            ((value.toInt & 0x0000FF00) >> 8).toByte,
            (value.toInt & 0x000000FF).toByte
          )
        })
    )

  override def length: BAW[UInt] = State(i => (i, UInt(4)))

  /**
    * Gets data sections if this data block has offsets
    *
    * @return an array of Data objects
    */
  override def data: BAW[List[Data]] = State(i => (i, List()))

}
