package tf.bug.scolor.otf.types.num

import cats.data.State
import scodec.bits.ByteVector
import tf.bug.scolor.table.SectionDataType
import tf.bug.scolor.{ByteAllocator, Data}
import spire.math.{UByte, UInt}
import tf.bug.scolor.implicits.BAW

case class OTFInt64(value: Long) extends SectionDataType {

  /**
    * @return an array of unsigned bytes representing the data.
    */
  override def bytes: BAW[ByteVector] =
    State(
      i =>
        (
          i,
          ByteVector(
            ((value & 0xFF00000000000000l) >> 64).toByte,
            ((value & 0x00FF000000000000l) >> 48).toByte,
            ((value & 0x0000FF0000000000l) >> 40).toByte,
            ((value & 0x000000FF00000000l) >> 32).toByte,
            ((value & 0x00000000FF000000l) >> 24).toByte,
            ((value & 0x0000000000FF0000l) >> 16).toByte,
            ((value & 0x000000000000FF00l) >> 8).toByte,
            (value & 0x00000000000000FFl).toByte
          )
      )
    )

  /**
    * Calculate/retrieve/return length in bytes of this data. Useful for if data needs to be allocated before it is calculated.
    *
    * @return an unsigned integer describing the length of this data block
    */
  override def length: BAW[UInt] = State(i => (i, UInt(8)))

  /**
    * Gets data sections if this data block has offsets. Used for if data needs to be allocated but can be in any location.
    *
    * @return an array of Data objects
    */
  override def data: BAW[List[Data]] = State(i => (i, List()))

}
