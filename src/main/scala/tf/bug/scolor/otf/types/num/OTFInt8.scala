package tf.bug.scolor.otf.types.num

import cats._
import cats.data._
import cats.implicits._
import scodec.bits.ByteVector
import tf.bug.scolor.implicits._
import tf.bug.scolor.{ByteAllocator, Data}
import tf.bug.scolor.table.SectionDataType
import spire.math.{UByte, UInt}

case class OTFInt8(value: Byte) extends SectionDataType {

  /**
    * @return an array of unsigned bytes representing the data.
    */
  override def bytes: BAW[ByteVector] = State(i => (i, ByteVector(value)))

  /**
    * Calculate/retrieve/return length in bytes of this data. Useful for if data needs to be allocated before it is calculated.
    *
    * @return an unsigned integer describing the length of this data block
    */
  override def length: BAW[UInt] = State(i => (i, UInt(1)))

  /**
    * Gets data sections if this data block has offsets. Used for if data needs to be allocated but can be in any location.
    *
    * @return an array of Data objects
    */
  override def data: BAW[List[Data]] = State(i => (i, List()))

}
