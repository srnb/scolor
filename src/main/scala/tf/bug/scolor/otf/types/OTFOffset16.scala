package tf.bug.scolor.otf.types

import cats.data.State
import scodec.bits.ByteVector
import tf.bug.scolor.otf.types.num.OTFUInt16
import tf.bug.scolor.{ByteAllocator, Data, Offset}
import spire.math.{UByte, UInt, UShort}
import tf.bug.scolor.implicits.BAW

case class OTFOffset16(offset: Int) extends Offset {

  override def position: UInt = UInt(offset)

  override def bytes: BAW[ByteVector] = OTFUInt16(UShort(offset)).bytes

  override def length: BAW[UInt] = State(i => (i, UInt(2)))

  /**
    * Gets data sections if this data block has offsets
    *
    * @return an array of Data objects
    */
  override def data: BAW[List[Data]] = State(i => (i, List()))

}
