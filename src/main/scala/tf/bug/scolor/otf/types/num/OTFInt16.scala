package tf.bug.scolor.otf.types.num

import cats.data.State
import scodec.bits.ByteVector
import tf.bug.scolor.{ByteAllocator, Data}
import tf.bug.scolor.table.SectionDataType
import spire.math.{UByte, UInt}
import tf.bug.scolor.implicits.BAW

case class OTFInt16(value: Short) extends SectionDataType {

  override def bytes: BAW[ByteVector] = State(i => (i, OTFInt16.bytes(value)))

  override def length: BAW[UInt] = State(i => (i, UInt(2)))

  /**
    * Gets data sections if this data block has offsets
    *
    * @return an array of Data objects
    */
  override def data: BAW[List[Data]] = State(i => (i, List()))

}

object OTFInt16 {

  def bytes(value: Short): ByteVector = {
    ByteVector(((value & 0xFF00) >> 8).toByte, (value & 0x00FF).toByte)
  }

}
