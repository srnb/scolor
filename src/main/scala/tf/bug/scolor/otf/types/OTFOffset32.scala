package tf.bug.scolor.otf.types

import cats.data.State
import scodec.bits.ByteVector
import tf.bug.scolor.otf.types.num.OTFUInt32
import tf.bug.scolor.{ByteAllocator, Data, Offset}
import spire.math.{UByte, UInt}
import tf.bug.scolor.implicits.BAW

case class OTFOffset32(offset: Long) extends Offset {

  override def position: UInt = UInt(offset)

  override def bytes: BAW[ByteVector] = OTFUInt32(UInt(offset)).bytes

  override def length: BAW[UInt] = State(i => (i, UInt(4)))

  /**
    * Gets data sections if this data block has offsets
    *
    * @return an array of Data objects
    */
  override def data: BAW[List[Data]] = State(i => (i, List()))

}
