val batikVersion = settingKey[String]("Batik Version")
val imageIOVersion = settingKey[String]("ImageIO Version")

lazy val commonSettings = Seq(
  organization   := "tf.bug",
  batikVersion   := "1.10",
  imageIOVersion := "3.4.1",
  scalacOptions += "-Ypartial-unification",
  resolvers += Resolver.bintrayRepo("alexknvl", "maven"),
)

lazy val core = (project in file(".")).settings(
  commonSettings,
  name         := "scolor",
  version      := "0.1.0",
  scalaVersion := "2.12.8",
  libraryDependencies ++= Seq(
    "org.typelevel" %% "spire"                      % "0.16.0",
    "org.typelevel" %% "cats-core"                  % "1.5.0",
    "org.scodec" %% "scodec-bits"                   % "1.1.6",
    "com.alexknvl" %% "polymorphic"                 % "0.4.0",
    "com.typesafe.scala-logging" %% "scala-logging" % "3.9.0",
    "org.apache.xmlgraphics"                        % "batik-svg-dom" % batikVersion.value,
    "org.apache.xmlgraphics"                        % "batik-transcoder" % batikVersion.value,
    "org.apache.xmlgraphics"                        % "batik-extension" % batikVersion.value,
    "org.apache.xmlgraphics"                        % "batik-rasterizer-ext" % batikVersion.value,
    "com.twelvemonkeys.imageio"                     % "imageio-batik" % imageIOVersion.value,
    "com.twelvemonkeys.imageio"                     % "imageio-core" % imageIOVersion.value,
    "com.twelvemonkeys.imageio"                     % "imageio-metadata" % imageIOVersion.value,
    "com.twelvemonkeys.common"                      % "common-lang" % imageIOVersion.value,
    "ch.qos.logback"                                % "logback-classic" % "1.2.3",
    "org.scalatest" %% "scalatest"                  % "3.0.5" % "test",
    "com.storm-enroute" %% "scalameter"             % "0.10" % "test",
  ),
  testFrameworks += new TestFramework("org.scalameter.ScalaMeterFramework"),
  logBuffered := false,
)
